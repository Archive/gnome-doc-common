README for gnome-doc-common
=============================

This module provides an easy way for packages to install their user
documentation in a consistent fashion. It also solves the problem of having to
manually update the various documentation build files to mirror every change to
the master copy.


Using gnome-doc-common
-----------------------
Using this module in another package is simple:

(1) Add a call to 'docmake --force' in your autogen.sh script.

(2) Call the AC_PROG_DOCMAKE macro as part of your configure.in or configure.ac
script.

(3) Add omf.make.in and xmldocs.make to the EXTRA_DIST target in your top-level
Makefile.am. You will usually also want to add these files to your top-level
.cvsignore file (along with omf.make).

What it does
-------------
The first step above will copy two files into your source directory. Namely,
omf.make.in and xmldocs.make.

The second step adds a --with-scrollkeeper option to the packages
configure script. This option provides the following possibilities:

--with-scrollkeeper or --with-scrollkeeper=yes
	This is the default setting.
	
	Updates the scrollkeeper database that is located at
	$(localstatedir)/lib/scrollkeeper. This should be a reasonable default
	location for most systems and will work smoothly with packaging scripts
	that install into a temporary directory (such as when creating rpms).

--with-scrollkeeper=no
	Do not try to update the scrollkeeper database as part of the install
	process.

--with-scrollkeeper=auto
	Updates the scrollkeeper database after working out the location of
	this database by calling the scrollkeeper-config program that comes
	with scrollkeeper. This option is useful if scrollkeeper has been built
	and installed in a non-standard location.

--with-scrollkeeper=path/to/scrollkeeper/db
	Updates the scrollkeeper database stored at the given path.

Generally, people building from source code will not need to use the
--with-scrollkeeper parameter, but it may be necessary in some cases (Gentoo
Linux seems to be a case where it will be necessary).

